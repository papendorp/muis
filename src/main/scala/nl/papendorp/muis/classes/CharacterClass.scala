package nl.papendorp.muis.classes

import cats.data.ValidatedNec
import cats.implicits._
import nl.papendorp.muis.rules._
import nl.papendorp.muis.xml.NodeChainOps.enrichedNodeChain
import nl.papendorp.muis.xml.{SpecificationError, ToplevelXML, XMLTag}

import scala.xml.Node

case class CharacterClass(
	name: String,
	baseAttackBonus: BaseAttackBonus,
	saves: Saves )


object ClassStatisticsBlock extends ToplevelXML
{
	override val MainTag: XMLTag = "class"

	val NameTag: XMLTag = "name"

	def apply( xml: Node )( implicit rules: RuleBook ): ValidatedNec[ SpecificationError, CharacterClass ] =
		validateMainTag( xml ).andThen( node =>
			(
				node.single( NameTag ).map( _.text ),
				BaseAttackBonusBlock( node ),
				SavesBlock( node )
			).mapN( CharacterClass ) )

}

case class InvalidBaseAttackBonusProgression( override val message: String ) extends SpecificationError