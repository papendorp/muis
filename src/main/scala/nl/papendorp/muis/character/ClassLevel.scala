package nl.papendorp.muis.character

import nl.papendorp.muis.classes.CharacterClass

case class ClassLevel( characterClass: CharacterClass )
