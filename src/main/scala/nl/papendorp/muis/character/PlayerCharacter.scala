package nl.papendorp.muis.character

import cats.data._
import cats.implicits._
import nl.papendorp.muis.race.Race
import nl.papendorp.muis.rules._
import nl.papendorp.muis.xml.NodeChainOps._
import nl.papendorp.muis.xml.{SpecificationError, ToplevelXML, XMLTag}

import scala.language.implicitConversions
import scala.xml.Node

case class PlayerCharacter(
	name: String,
	alignment: Alignment,
	race: Race,
	progression: Seq[ ClassLevel ]
)

object CharacterSheet extends ToplevelXML
{
	override val MainTag: XMLTag = "character"

	val NameTag: XMLTag = "name"
	val RaceTag: XMLTag = "race"
	val ProgressionTag: XMLTag = "progression"
	val LevelTag: XMLTag = "level"

	def apply( xml: Node )( implicit rules: RuleBook ): ValidatedNec[ SpecificationError, PlayerCharacter ] =
	{
		validateMainTag( xml ) andThen (node =>
			(
				node.single( NameTag ).map( _.text ),
				AlignmentBlock( node ),
				node.single( RaceTag ) andThen retrieveRace _,
				node.single( ProgressionTag ) andThen parseLevels
			).mapN( PlayerCharacter ))
	}

	def retrieveRace( xml: Node )( implicit rules: RuleBook ): ValidatedNec[ SpecificationError, Race ] =
		rules.race( xml.text ) match {
			case None => MissingRace( xml.text ).invalidNec
			case Some( race ) => race.validNec
		}

	def parseLevels( xml: Node )( implicit rules: RuleBook ): ValidatedNec[ SpecificationError, Seq[ ClassLevel ] ] =
	{
		val maybeClasses = for {
			level <- xml \ LevelTag
			className = level.text
		} yield (className, rules.characterClass( className ))

		maybeClasses.map{
			case (_, Some( c )) => ClassLevel( c ).validNec
			case (className, None) => MissingCharacterClass( className ).invalidNec
		}.toList traverse identity
	}
}

case class MissingRace( override val message: String ) extends SpecificationError

case class MissingCharacterClass( override val message: String ) extends SpecificationError
