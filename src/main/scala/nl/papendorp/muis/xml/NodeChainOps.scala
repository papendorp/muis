package nl.papendorp.muis.xml

import cats.data.ValidatedNec
import cats.implicits._

import scala.language.implicitConversions
import scala.xml.{Node, Utility}

class NodeChainOps( xml: Seq[ Node ] )
{
	def single( tag: XMLTag ): ValidatedNec[ SpecificationError, Node ] = xml \ tag match {
		case Seq() => MissingTag( tag ).invalidNec
		case Seq( x ) => x.validNec
		case _ => DuplicateTag( tag ).invalidNec
	}
}

object NodeChainOps
{
	implicit def enrichedNodeChain( xml: Seq[ Node ] ): NodeChainOps = new NodeChainOps( xml )
}

trait ToplevelXML
{
	val MainTag: XMLTag

	def validateMainTag( xml: Node ): ValidatedNec[ SpecificationError, Node ] =
		if( xml.label == MainTag.name )
			Utility.trim( xml ).validNec
		else
			InvalidMainTag( xml.label, MainTag ).invalidNec
}

case class MissingTag( missingTag: XMLTag ) extends SpecificationError
{
	override val message: String = missingTag
}

case class DuplicateTag( duplicateTag: XMLTag ) extends SpecificationError
{
	override val message: String = duplicateTag
}

case class InvalidMainTag( found: XMLTag, expectedMain: XMLTag ) extends SpecificationError
{
	override val message: String = s"${found.name} should be ${expectedMain.name}"
}
