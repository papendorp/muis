package nl.papendorp.muis.xml

import cats.implicits._

import scala.language.implicitConversions

object XMLTag
{
	implicit def fromString( name: String ): XMLTag = new XMLTag( name )

	implicit def asString( tag: XMLTag ): String = tag.name

	def unapply( tag: XMLTag ): Option[ String ] = tag.name.some
}

case class XMLTag( name: String )
