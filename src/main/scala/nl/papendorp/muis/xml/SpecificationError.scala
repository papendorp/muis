package nl.papendorp.muis.xml

import cats.Eq

trait SpecificationError
{
	def message: String

	override def toString: String = s"${getClass.getSimpleName}( $message )"
}

object SpecificationError
{
	implicit def eq: Eq[ SpecificationError ] = ( x: SpecificationError, y: SpecificationError ) => x.equals( y )
}