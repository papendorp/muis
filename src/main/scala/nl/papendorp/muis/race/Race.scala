package nl.papendorp.muis.race

import cats.data._
import cats.implicits._
import nl.papendorp.muis.rules._
import nl.papendorp.muis.xml.NodeChainOps._
import nl.papendorp.muis.xml.{SpecificationError, ToplevelXML, XMLTag}

import scala.xml.Node

case class Race( name: String, size: Size, speed: Speed )

object RaceStatisticsBlock extends ToplevelXML
{
	override val MainTag: XMLTag = "race"

	val NameTag: XMLTag = "name"

	def apply( xml: Node )( implicit book: RuleBook ): ValidatedNec[ SpecificationError, Race ] =
	{
		validateMainTag( xml ) andThen (raceXML =>
			(
				raceXML.single( NameTag ).map( _.text ),
				SizeBlock( raceXML ),
				SpeedBlock( raceXML )
			).mapN( Race ))
	}
}
