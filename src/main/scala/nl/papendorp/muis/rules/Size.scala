package nl.papendorp.muis.rules

import cats.data.ValidatedNec
import cats.implicits._
import nl.papendorp.muis.xml.NodeChainOps._
import nl.papendorp.muis.xml.{SpecificationError, XMLTag}

import scala.xml.Node

sealed case class Size( name: String )

object Size
{
	val Diminutive: Size = Size( "diminutive" )
	val Tiny: Size = Size( "tiny" )
	val Small: Size = Size( "small" )
	val Medium: Size = Size( "medium" )
	val LargeTall: Size = Size( "large (tall)" )
	val LargeLong: Size = Size( "large (long)" )
	val HugeTall: Size = Size( "huge (tall)" )
	val HugeLong: Size = Size( "huge (long)" )
	val GargantuanTall: Size = Size( "gargantuan (tall)" )
	val GargantuanLong: Size = Size( "gargantuan (long)" )
	val ColossalTall: Size = Size( "colossal (tall)" )
	val ColossalLong: Size = Size( "colossal (long)" )

	val sizes: Seq[ Size ] = List( Diminutive, Tiny, Small, Medium, LargeTall, LargeLong, HugeTall, HugeLong,
		GargantuanTall, GargantuanLong, ColossalTall, ColossalLong )

	lazy val forName: String => Option[ Size ] = sizes.map( size => size.name -> size ).toMap.get
}

object SizeBlock
{
	val SizeTag: XMLTag = "size"

	def apply( xml: Node ): ValidatedNec[ SpecificationError, Size ] =
		xml.single( SizeTag ) andThen (node => Size.forName( node.text ).
			map( _.validNec ).
			getOrElse( InvalidSize( node.text ).invalidNec ))
}

case class InvalidSize( override val message: String ) extends SpecificationError