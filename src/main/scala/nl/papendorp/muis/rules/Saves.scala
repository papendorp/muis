package nl.papendorp.muis.rules

import cats.data.ValidatedNec
import cats.implicits._
import nl.papendorp.muis.xml.NodeChainOps._
import nl.papendorp.muis.xml.{SpecificationError, XMLTag}

import scala.xml.Node

case class Saves(
	fortitude: SaveProgression,
	reflex: SaveProgression,
	will: SaveProgression )

object SavesBlock extends
{
	val SavesTag: XMLTag = "saves"

	val FortitudeTag: XMLTag = "fortitude"
	val ReflexTag: XMLTag = "reflex"
	val WillTag: XMLTag = "will"

	def apply( xml: Node ): ValidatedNec[ SpecificationError, Saves ] =
	{
		xml.single( SavesTag ) andThen (savesTag =>
			(
				progressionFrom( savesTag, FortitudeTag ),
				progressionFrom( savesTag, ReflexTag ),
				progressionFrom( savesTag, WillTag )
			).mapN( Saves ))
	}

	def progressionFrom( savesTag: Node, progressionTag: XMLTag ): ValidatedNec[ SpecificationError, SaveProgression ] =
		savesTag.single( progressionTag ) andThen (saveTag => SaveProgression.fromName( saveTag.text ).
			map( _.validNec ).
			getOrElse( InvalidSaveProgression( saveTag.text ).invalidNec ))
}

sealed abstract class SaveProgression( val name: String )

object SaveProgression
{

	case object HighSaveProgression extends SaveProgression( "high" )

	case object LowSaveProgression extends SaveProgression( "low" )

	def fromName( name: String ): Option[ SaveProgression ] = name.trim.toLowerCase match {
		case "high" => Some( HighSaveProgression )
		case "low" => Some( LowSaveProgression )
		case _ => None
	}
}

sealed case class InvalidSaveProgression( message: String ) extends SpecificationError