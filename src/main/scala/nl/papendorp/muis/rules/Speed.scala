package nl.papendorp.muis.rules

import cats.data.ValidatedNec
import cats.implicits._
import nl.papendorp.muis.xml.NodeChainOps._
import nl.papendorp.muis.xml.{SpecificationError, XMLTag}

import scala.xml.Node

case class Speed( speedInFoot: Int )

object SpeedBlock
{
	val SpeedTag: XMLTag = "speed"

	def apply( xml: Node ): ValidatedNec[ SpecificationError, Speed ] =
		xml.single( SpeedTag ) andThen (node =>
			try {
				val speed = node.text.toInt
				if( speed < 0 )
					InvalidSpeed( node.text ).invalidNec
				else
					Speed( speed ).validNec
			}
			catch {
				case _: NumberFormatException => InvalidSpeed( node.text ).invalidNec
			})
}

case class InvalidSpeed( override val message: String ) extends SpecificationError
