package nl.papendorp.muis.rules

import cats.data.ValidatedNec
import cats.implicits._
import nl.papendorp.muis.classes.InvalidBaseAttackBonusProgression
import nl.papendorp.muis.xml.NodeChainOps.enrichedNodeChain
import nl.papendorp.muis.xml.{SpecificationError, XMLTag}

import scala.xml.Node

sealed abstract class BaseAttackBonus private( val name: String, val progression: Float )

object BaseAttackBonus
{

	case object HighProgression extends BaseAttackBonus( "high", 1.0F )

	case object MediumProgression extends BaseAttackBonus( "medium", 0.75F )

	case object LowProgression extends BaseAttackBonus( "low", 0.5F )

	def forName( name: String ): Option[ BaseAttackBonus ] = name.toLowerCase match {
		case "high" => Some( HighProgression )
		case "medium" => Some( MediumProgression )
		case "low" => Some( LowProgression )
		case _ => None
	}
}

object BaseAttackBonusBlock
{
	val BABTag: XMLTag = "BAB"

	def apply( xml: Node ): ValidatedNec[ SpecificationError, BaseAttackBonus ] =
		xml.single( BABTag ) andThen (node => BaseAttackBonus.forName( node.text ).
			map( _.validNec ).
			getOrElse( InvalidBaseAttackBonusProgression( node.text ).invalidNec ))

}