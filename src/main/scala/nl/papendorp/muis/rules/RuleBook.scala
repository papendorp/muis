package nl.papendorp.muis.rules

import cats.data.ValidatedNec
import cats.implicits._
import nl.papendorp.muis.classes.CharacterClass
import nl.papendorp.muis.race.Race
import nl.papendorp.muis.rules.DuplicateClassName._
import nl.papendorp.muis.rules.DuplicateRaceName._
import nl.papendorp.muis.xml.SpecificationError

import scala.language.implicitConversions

case class RuleBook private(
	/*private val*/ races: Map[ String, Race ],
	/*private val*/ characterClasses: Map[ String, CharacterClass ]
)
{
	def race( name: String ): Option[ Race ] = races.get( name )

	def characterClass( name: String ): Option[ CharacterClass ] = characterClasses.get( name )

	def baseAttackBonusProgression( name: String ): Option[ BaseAttackBonus ] = BaseAttackBonus.forName( name )

	def size( name: String ): Option[ Size ] = Size.forName( name )

	def withClasses( classes: CharacterClass* ): ValidatedNec[ SpecificationError, RuleBook ] =
		uniqueBy( classes, ( c: CharacterClass ) => c.name ).
			map( RuleBook(
				races,
				_
			) )

	def withRaces( races: Race* ): ValidatedNec[ SpecificationError, RuleBook ] =
		uniqueBy( races, ( r: Race ) => r.name ).
			map( RuleBook(
				_,
				characterClasses
			) )

	private def uniqueBy[ KEY, CONTENTS, ERROR <: SpecificationError ]( seq: Seq[ CONTENTS ], key: CONTENTS => KEY )
		( implicit error: CONTENTS => ERROR ): ValidatedNec[ SpecificationError, Map[ KEY, CONTENTS ] ] =
		seq.foldLeft( Map[ KEY, CONTENTS ]().validNec[ SpecificationError ] )( ( validatedMap, right ) =>
			validatedMap andThen (
				map =>
					if( map.contains( key( right ) ) )
						error( right ).invalidNec
					else
						map.updated( key( right ), right ).validNec
				)
		)
}

object RuleBook
{
	implicit val DefaultBook: RuleBook = RuleBook(
		races = Map(),
		characterClasses = Map()
	)

	def apply( ): RuleBook = DefaultBook
}

object DuplicateClassName
{
	implicit def fromCharacterClass( characterClass: CharacterClass ): DuplicateClassName = DuplicateClassName( characterClass.name )
}

case class DuplicateClassName( override val message: String ) extends SpecificationError

object DuplicateRaceName
{
	implicit def fromRace( race: Race ): DuplicateRaceName = DuplicateRaceName( race.name )
}

case class DuplicateRaceName( override val message: String ) extends SpecificationError

