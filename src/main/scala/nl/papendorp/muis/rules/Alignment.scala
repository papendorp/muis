package nl.papendorp.muis.rules

import cats.data.ValidatedNec
import cats.implicits._
import nl.papendorp.muis.rules.AlignmentComponent.{beatitude, order}
import nl.papendorp.muis.xml.NodeChainOps._
import nl.papendorp.muis.xml.{SpecificationError, XMLTag}

import scala.language.{implicitConversions, postfixOps}
import scala.xml.NodeSeq

case class AlignmentComponent private( description: String )

object AlignmentComponent
{
	def beatitude( component: String ): ValidatedNec[ SpecificationError, AlignmentComponent ] = component match {
		case "Good" | "Neutral" | "Evil" => AlignmentComponent( component ).validNec
		case _ => InvalidAlignment( component ).invalidNec
	}

	def order( component: String ): ValidatedNec[ SpecificationError, AlignmentComponent ] = component match {
		case "Lawful" | "Neutral" | "Chaotic" => AlignmentComponent( component ).validNec
		case _ => InvalidAlignment( component ).invalidNec
	}
}

case class Alignment( order: AlignmentComponent, beautitude: AlignmentComponent )
{
	val text = s"${order.description} ${beautitude.description}"
}

object AlignmentBlock
{
	val AlignmentTag: XMLTag = "alignment"

	def apply( xml: NodeSeq ): ValidatedNec[ SpecificationError, Alignment ] =
		xml.single( AlignmentTag ) andThen { tag => fromString( tag.text ) }

	def fromString( tagText: String ): ValidatedNec[ SpecificationError, Alignment ] = tagText.split( " " ) match {
		case Array( o, b ) => (order( o ), beatitude( b )) mapN Alignment
		case _ => InvalidAlignment( tagText ).invalidNec
	}
}

case class InvalidAlignment( override val message: String ) extends SpecificationError
