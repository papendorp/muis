package nl.papendorp.muis.ports

import cats.data.ValidatedNec
import cats.implicits._
import nl.papendorp.muis.classes.{CharacterClass, ClassStatisticsBlock}
import nl.papendorp.muis.race.{Race, RaceStatisticsBlock}
import nl.papendorp.muis.rules.RuleBook
import nl.papendorp.muis.xml.SpecificationError

import scala.xml.Elem

object Initializer
{
	def loadRules( xmlResource: XMLResource ): ValidatedNec[ SpecificationError, RuleBook ] =
		parseBoth( xmlResource.raceXMLs, xmlResource.classXMLs ) andThen {
			case (races, characterClasses) => RuleBook().withRaces( races: _* ).
				andThen( _.withClasses( characterClasses: _* ) )
		}

	private def parseBoth( raceXMLs: RaceXMLs, classXMLs: ClassXMLs ) =
		(raceXMLs.parseAll, classXMLs.parseAll).tupled
}

case class XMLResource(
	raceXMLs: RaceXMLs,
	classXMLs: ClassXMLs,
	characterXMLs: Seq[ Elem ]
)

sealed trait ParsableList[ RESULT ]
{
	def xmls: Seq[ Elem ]

	def parser: Elem => ValidatedNec[ SpecificationError, RESULT ]

	def parseAll: ValidatedNec[ SpecificationError, List[ RESULT ] ] =
		xmls.map( parser ).toList.sequence
}

case class RaceXMLs( xmls: Seq[ Elem ] ) extends ParsableList[ Race ]
{
	override val parser = RaceStatisticsBlock( _ )
}

case class ClassXMLs( xmls: Seq[ Elem ] ) extends ParsableList[ CharacterClass ]
{
	override val parser = ClassStatisticsBlock( _ )
}