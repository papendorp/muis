package nl.papendorp.muis.ports

import java.io.File

import scala.io.Source
import scala.xml.{Elem, XML}

class FileScanner(
	raceResourceDir: String,
	classResourceDir: String,
	characterResourceDir: String )
{
	val ResourceRoot = new File( getClass.getClassLoader.getResource( "." ).getFile )

	def scan: XMLResource = XMLResource(
		RaceXMLs( loadFiles( raceResourceDir ) ),
		ClassXMLs( loadFiles( classResourceDir ) ),
		characterXMLs = loadFiles( characterResourceDir )
	)

	private def loadFiles( directoryName: String ): Seq[ Elem ] = for {
		file <- new File( ResourceRoot, directoryName ).listFiles
		if file.isFile
	} yield loadFile( s"$directoryName/${file.getName}" )

	private def loadFile( fileName: String ): Elem =
	{
		val characterSource: String = Source.fromResource( fileName ).mkString
		XML.loadString( characterSource )
	}
}
