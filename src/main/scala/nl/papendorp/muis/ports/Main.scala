package nl.papendorp.muis.ports

import cats.data.Validated.{Invalid, Valid}
import cats.data.ValidatedNec
import cats.implicits._
import nl.papendorp.muis.character.{CharacterSheet, PlayerCharacter}
import nl.papendorp.muis.xml.SpecificationError

object Main extends App
{
	val RaceResources = "races"
	val ClassResources = "classes"
	val CharacterResources = "characters"

	val fileScanner = new FileScanner(
		raceResourceDir = RaceResources,
		classResourceDir = ClassResources,
		characterResourceDir = CharacterResources
	)

	val playerCharacters: ValidatedNec[ SpecificationError, List[ PlayerCharacter ] ] = {
		val xmlResources = fileScanner.scan

		Initializer.loadRules( xmlResources ).andThen( ruleBook => {
			val playerCharacters = for {
				playerCharacterXML <- xmlResources.characterXMLs
			} yield CharacterSheet( playerCharacterXML )( ruleBook )

			playerCharacters.toList.sequence
		} )
	}

	Initializer.loadRules( fileScanner.scan ) foreach println
	playerCharacters match {
		case Valid( list ) => list foreach println
		case Invalid( errors ) => println( errors )
	}
}
