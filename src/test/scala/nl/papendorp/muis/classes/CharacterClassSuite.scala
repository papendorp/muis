package nl.papendorp.muis.classes

import cats.data.ValidatedNec
import nl.papendorp.muis.classes.CharacterClassXML.xmlGenerator
import nl.papendorp.muis.rules._
import nl.papendorp.muis.testresource.catsextensions.ValidationOps._
import nl.papendorp.muis.testresource.xml.{MockXML, ParserChecker, SingleElementTest}
import nl.papendorp.muis.xml.{InvalidMainTag, SpecificationError}
import org.scalacheck.Gen
import org.scalacheck.Gen.asciiStr

import scala.xml.Elem

trait MockCharacterClass
	extends MockSaves
		with MockBaseAttackBonus
{
	val mockClasses: Gen[ CharacterClass ] = for {
		anyClassName <- asciiStr
		anyBABProgression <- anyBABProgression
		anySavesProgression <- anySaves
	} yield CharacterClass( anyClassName, anyBABProgression, anySavesProgression )
}

class CharacterClassSuite
	extends ParserChecker[ CharacterClassXML, CharacterClass ]
		with SingleElementTest
		with MockSaveNames
		with MockBaseAttackBonusNames
{
	override val emptyXML = <class/>

	implicit val actual = ClassStatisticsBlock( _ )

	override def actual( mockXML: CharacterClassXML ): ValidatedNec[ SpecificationError, CharacterClass ] =
		ClassStatisticsBlock( mockXML.xml )

	"A character class" when {
		"loaded from invalid XML" should {

			"verify main tag" in {
				actual( <incorrect/> ) reports InvalidMainTag( "incorrect", "class" )
			}

			for( tagName <- Seq( "name", "BAB", "saves" ) ) {
				ensuresSingleElementOf( tagName )
			}
		}

		"loaded from valid XML" should {
			"read name" in {
				checkProperty( _.name == _.expectedName )
			}
		}

		"parsing BAB" should {
			"retrieve base attack bonus by name" in {
				checkProperty( ( characterClass, xml ) =>
					RuleBook().baseAttackBonusProgression( xml.expectedBaseAttackBonus ).
						forall( _ == characterClass.baseAttackBonus ) )
			}

			"report invalid base attack bonus progression" in {
				implicit val invalidBAB: Gen[ CharacterClassXML ] = xmlGenerator( babNames = invalidBABNames )

				reportsUsing( xml => InvalidBaseAttackBonusProgression( xml.expectedBaseAttackBonus ) )
			}
		}

		"parsing saves by name" should {
			"retrieve fortitude progression" in {
				checkProperty( _.saves.fortitude.name == _.expectedSaves.fortitude )
			}

			"retrieve reflex progression" in {
				checkProperty( _.saves.reflex.name == _.expectedSaves.reflex )
			}

			"retrieve will progression" in {
				checkProperty( _.saves.will.name == _.expectedSaves.will )
			}

			implicit val invalidSaves = xmlGenerator( saveNames = invalidSaveNames )

			"report invalid fortitude progression" in {
				reportsUsing( xml => InvalidSaveProgression( xml.expectedSaves.fortitude ) )
			}

			"report invalid reflex progression" in {
				reportsUsing( xml => InvalidSaveProgression( xml.expectedSaves.reflex ) )
			}

			"report invalid will progression" in {
				reportsUsing( xml => InvalidSaveProgression( xml.expectedSaves.will ) )
			}
		}
	}
}

case class CharacterClassXML(
	expectedName: String,
	expectedBaseAttackBonus: String,
	expectedSaves: SaveNames
) extends MockXML
{
	override def xml: Elem =
		<class>
			<name>
				{expectedName}
			</name>
			<BAB>
				{expectedBaseAttackBonus}
			</BAB>

			<saves>
				<fortitude>
					{expectedSaves.fortitude}
				</fortitude>
				<reflex>
					{expectedSaves.reflex}
				</reflex>
				<will>
					{expectedSaves.will}
				</will>
			</saves>

		</class>
}

object CharacterClassXML extends MockBaseAttackBonusNames with MockSaveNames
{
	def xmlGenerator(
		names: Gen[ String ] = asciiStr,
		babNames: Gen[ String ] = validBABNames,
		saveNames: Gen[ SaveNames ] = validSaveNames
	): Gen[ CharacterClassXML ] = for {
		anyName <- names
		validBABName <- babNames
		validSaveNames <- saveNames
	} yield CharacterClassXML( anyName, validBABName, validSaveNames )

	implicit val validXMLs: Gen[ CharacterClassXML ] = xmlGenerator()
}