package nl.papendorp.muis.race

import cats.data.ValidatedNec
import nl.papendorp.muis.race.RaceXML.xmlGenerator
import nl.papendorp.muis.rules._
import nl.papendorp.muis.testresource.catsextensions.ValidationOps._
import nl.papendorp.muis.testresource.xml.{MockXML, ParserChecker, SingleElementTest}
import nl.papendorp.muis.xml.{InvalidMainTag, SpecificationError}
import org.scalacheck.Gen
import org.scalacheck.Gen.asciiStr

import scala.util.Try
import scala.xml.{Elem, Node}

trait MockRace
	extends MockSpeed
		with MockSize
{
	val validRaces: Gen[ Race ] = for {
		anyName <- asciiStr
		anySize <- mockSizes
		anySpeed <- mockSpeeds
	} yield Race( anyName, anySize, anySpeed )
}

class RaceSuite
	extends ParserChecker[ RaceXML, Race ]
		with SingleElementTest
		with MockSizeNames
		with MockSpeedTexts
{
	override val emptyXML: Elem = <race/>
	implicit val actual: Node => ValidatedNec[ SpecificationError, Race ] = ( node: Node ) => RaceStatisticsBlock( node )

	override def actual( raceXML: RaceXML ): ValidatedNec[ SpecificationError, Race ] = RaceStatisticsBlock( raceXML.xml )

	"A Race" when {
		"loaded from incomplete XML" should {

			"complete without exception" in {
				assert( Try( actual( emptyXML ) ).isSuccess )
			}

			"verify main tag" in {
				actual( <incorrect/> ) reports InvalidMainTag( "incorrect", "race" )
			}

			for( tagName <- Seq( "name", "size", "speed" ) ) {
				ensuresSingleElementOf( tagName )
			}
		}

		"parsed from valid xml" should {
			"contain its name" in {
				checkProperty( _.name == _.expectedName )
			}
		}

		"parsing size" should {
			"accept valid sizes" in {
				checkProperty( ( race, xml ) =>
					Size.forName( xml.expectedSizeName ).forall( _ == race.size ) )
			}

			"report invalid size" in {
				reportsUsing( xml => InvalidSize( xml.expectedSizeName ) )( xmlGenerator( sizeNames = invalidSizeNames ) )
			}
		}

		"parsing speed" should {
			"accept valid speeds" in {
				checkProperty( _.speed.speedInFoot.toString === _.expectedSpeedText )
			}

			"report non-numeric speeds" in {
				reportsUsing( xml => InvalidSpeed( xml.expectedSpeedText ) )( xmlGenerator( speedTexts = nonNumericSpeedTexts ) )
			}

			"report negative speeds" in {
				reportsUsing( xml => InvalidSpeed( xml.expectedSpeedText ) )( xmlGenerator( speedTexts = negativeSpeedTexts ) )
			}
		}
	}
}

case class RaceXML(
	expectedName: String,
	expectedSizeName: String,
	expectedSpeedText: String
) extends MockXML
{
	override lazy val xml: Elem =
		<race>
			<name>
				{expectedName}
			</name>
			<size>
				{expectedSizeName}
			</size>
			<speed>
				{expectedSpeedText}
			</speed>
		</race>
}

object RaceXML
	extends MockSizeNames
		with MockSpeedTexts
{
	def xmlGenerator(
		names: Gen[ String ] = asciiStr,
		sizeNames: Gen[ String ] = validSizeNames,
		speedTexts: Gen[ String ] = validSpeedTexts
	): Gen[ RaceXML ] = for {
		anyName <- names
		anySizeName <- sizeNames
		anySpeedText <- speedTexts
	} yield RaceXML( anyName, anySizeName, anySpeedText )

	implicit val validXMLs: Gen[ RaceXML ] = xmlGenerator()
}