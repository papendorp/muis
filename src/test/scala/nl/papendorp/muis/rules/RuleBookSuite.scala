package nl.papendorp.muis.rules

import nl.papendorp.muis.classes.{CharacterClass, MockCharacterClass}
import nl.papendorp.muis.race.{MockRace, Race}
import nl.papendorp.muis.testresource.catsextensions.ValidationOps._
import nl.papendorp.muis.testresource.{MockGenerators, TestConfig}
import nl.papendorp.muis.xml.SpecificationError._
import org.scalacheck.Gen
import org.scalacheck.Prop.{BooleanOperators, forAll}

trait MockRuleBooks
	extends TestConfig
		with MockGenerators
		with MockCharacterClass
		with MockRace
{
	val mockRuleBooks: Gen[ RuleBook ] = for {
		races <- uniqueBy[ Race ]( validRaces, ( race: Race ) => race.name ).map( ( set: Set[ Race ] ) => set.toList )
		classes <- uniqueBy[ CharacterClass ]( mockClasses, ( characterClass: CharacterClass ) => characterClass.name ).map( ( set: Set[ CharacterClass ] ) => set.toList )
		ruleBookValidation = RuleBook().withRaces( races: _* ).andThen( _.withClasses( classes: _* ) )
		if ruleBookValidation.isValid
	} yield ruleBookValidation.getOrElse( throw new IllegalStateException )
}

class RuleBookSuite
	extends TestConfig
		with MockBaseAttackBonusNames
		with MockCharacterClass
		with MockRace
{
	"A rule book" when {
		"unmodified" should {
			"contain all sizes" in {
				for( size <- Size.sizes )
					RuleBook().size( size.name ) should be( Some( size ) )
			}

			"contain no invalid sizes" in {
				val validSizeNames = Size.sizes.map( _.name ).toSet

				check( forAll( ( invalidSizeName: String ) =>
					!validSizeNames.contains( invalidSizeName ) ==> RuleBook().size( invalidSizeName ).isEmpty
				) )
			}

			"contains all base attack bonus progressions" in {
				for( progressionName <- ValidBABNames )
					RuleBook().baseAttackBonusProgression( progressionName ).map( _.name ) should be( Some( progressionName ) )
			}

			"contains no invalid base attack bonus progression" in {
				check( forAll( invalidBABNames )( RuleBook().baseAttackBonusProgression( _ ).isEmpty ) )
			}
		}

		"adding character classes" should {
			"report duplicate names" in {
				check( forAll( mockClasses )( ( characterClass: CharacterClass ) => {
					val validation = RuleBook().withClasses( characterClass, characterClass )
					validation contains DuplicateClassName( characterClass.name )
				} ) )
			}
		}

		"adding races" should {
			"report duplicate names" in {
				check( forAll( validRaces )( ( race: Race ) => {
					val validation = RuleBook().withRaces( race, race )
					validation contains DuplicateRaceName( race.name )
				} ) )
			}
		}
	}
}
