package nl.papendorp.muis.rules

import org.scalacheck.Gen
import org.scalacheck.Gen.{asciiStr, oneOf}

trait AlignmentComponentTexts
{
	val orderComponentNames: Gen[ String ] = oneOf( "Lawful", "Neutral", "Chaotic" )
	val orderComponents: Gen[ AlignmentComponent ] = orderComponentNames.map( AlignmentComponent( _ ) )

	val beatitudeComponentNames: Gen[ String ] = oneOf( "Good", "Neutral", "Evil" )
	val beatitudeComponents: Gen[ AlignmentComponent ] = beatitudeComponentNames.map( AlignmentComponent( _ ) )
}

trait MockAlignment extends AlignmentComponentTexts
{
	val mockAlignments: Gen[ Alignment ] = for {
		order <- orderComponents
		beatitude <- beatitudeComponents
	} yield Alignment( order, beatitude )
}

trait MockAlignmentText extends AlignmentComponentTexts
{
	val validAlignmentTexts: Gen[ String ] = for {
		order <- orderComponentNames
		beatitude <- beatitudeComponentNames
	} yield s"$order $beatitude"

	val invalidAlignmentTexts: Gen[ String ] = for {
		anyText <- asciiStr
		if !(anyText contains ' ')
		if Set( "Good", "Neutral", "Evil", "Lawful", "Chaotic" ).forall( !anyText.contains( _ ) )
	} yield anyText
}

class AlignmentSuite
{

}
