package nl.papendorp.muis.rules

import org.scalacheck.Gen

trait MockSize
{
	val mockSizes: Gen[ Size ] = Gen.oneOf( Size.sizes )
}

trait MockSizeNames
{
	val ValidSizeNames: Seq[ String ] = Size.sizes.map( _.name )
	val validSizeNames: Gen[ String ] = Gen.oneOf( ValidSizeNames )
	val invalidSizeNames: Gen[ String ] = for( name <- Gen.asciiStr if !ValidSizeNames.contains( name ) ) yield name
}
