package nl.papendorp.muis.rules

import org.scalacheck.Gen
import org.scalacheck.Gen.{asciiStr, choose}

import scala.util.Try

trait MockSpeed
{
	val mockSpeeds: Gen[ Speed ] = for {
		fiveFootIncrement <- choose( 0, 200 )
	} yield Speed( fiveFootIncrement * 5 )
}

trait MockSpeedTexts
{
	val validSpeedTexts: Gen[ String ] = for( speedInFoot <- choose( 0, 1000 ) ) yield speedInFoot.toString

	val nonNumericSpeedTexts: Gen[ String ] = for( speedText <- asciiStr if Try( speedText.toInt ).isFailure ) yield speedText

	val negativeSpeedTexts: Gen[ String ] = for( negativeSpeed <- choose( Integer.MIN_VALUE, -1 ) ) yield negativeSpeed.toString
}

class SpeedSuite
{

}
