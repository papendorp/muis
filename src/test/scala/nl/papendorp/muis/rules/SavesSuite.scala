package nl.papendorp.muis.rules

import nl.papendorp.muis.rules.SaveProgression.{HighSaveProgression, LowSaveProgression}
import org.scalacheck.Gen
import org.scalacheck.Gen.oneOf

trait MockSaves
{
	val anySaveProgression: Gen[ SaveProgression ] = oneOf( HighSaveProgression, LowSaveProgression )

	val anySaves: Gen[ Saves ] = for {
		anyFortitude <- oneOf( HighSaveProgression, LowSaveProgression )
		anyReflex <- oneOf( HighSaveProgression, LowSaveProgression )
		anyWill <- oneOf( HighSaveProgression, LowSaveProgression )
	} yield Saves( anyFortitude, anyReflex, anyWill )
}

case class SaveNames( fortitude: String, reflex: String, will: String )

trait MockSaveNames
{

	private val ValidNames = Seq( "high", "low" )

	val invalidSaveNames: Gen[ SaveNames ] =
		saveNameGenFrom( for( name <- Gen.asciiStr if !(ValidNames contains name) ) yield name )

	val validSaveNames: Gen[ SaveNames ] = saveNameGenFrom( oneOf( ValidNames ) )

	private def saveNameGenFrom( nameGen: Gen[ String ] ): Gen[ SaveNames ] = for {
		fortitude <- nameGen
		reflex <- nameGen
		will <- nameGen
	} yield SaveNames( fortitude, reflex, will )

}
