package nl.papendorp.muis.rules

import nl.papendorp.muis.rules.BaseAttackBonus.{HighProgression, LowProgression, MediumProgression}
import org.scalacheck.Gen
import org.scalacheck.Gen.oneOf

trait MockBaseAttackBonus
{
	val anyBABProgression: Gen[ BaseAttackBonus ] = oneOf( HighProgression, MediumProgression, LowProgression )
}

trait MockBaseAttackBonusNames
{
	val ValidBABNames: Seq[ String ] = Seq( "high", "medium", "low" )
	val invalidBABNames: Gen[ String ] = for( name <- Gen.asciiStr if !(ValidBABNames contains name) ) yield name
	val validBABNames: Gen[ String ] = oneOf( ValidBABNames )
}
