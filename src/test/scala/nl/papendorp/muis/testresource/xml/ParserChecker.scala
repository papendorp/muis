package nl.papendorp.muis.testresource.xml

import cats.data.Validated.{Invalid, Valid}
import cats.data.ValidatedNec
import nl.papendorp.muis.testresource.TestConfig
import nl.papendorp.muis.xml.SpecificationError
import org.scalacheck.Gen
import org.scalacheck.Prop.{BooleanOperators, forAll}
import org.scalatest.Assertion

import scala.language.implicitConversions
import scala.xml.Elem

trait ParserChecker[ MOCK_XML <: MockXML, RESULT ] extends TestConfig
{
	def actual( xml: MOCK_XML ): ValidatedNec[ SpecificationError, RESULT ]

	def checkProperty( matchProperty: (RESULT, MOCK_XML) => Boolean )( implicit mockXMLs: Gen[ MOCK_XML ] ): Unit =
		check( forAll( mockXMLs ){ mockXML =>
		val result = actual( mockXML )

		s"""
			 |***********************************
			 |*** [$result]
			 |*** did not match property
			 |***********************************
			 |""".stripMargin |:
			(result match {
				case Valid( validResult ) => matchProperty( validResult, mockXML )
				case _ => false
			})
		} )

	def reportsUsing( errorFrom: MOCK_XML => SpecificationError )( implicit xmls: Gen[ MOCK_XML ] ): Assertion =
		check( forAll( xmls ){ xml =>
		val result = actual( xml )
		val expectedError = errorFrom( xml )

		s"""
			 |***********************************
			 |*** [$result]
			 |*** did not contain
			 |*** [$expectedError]
			 |***********************************
			 |""".stripMargin |:
			(result match {
				case Invalid( errors ) => errors contains expectedError
				case _ => false
			})
		} )
}

trait MockXML
{
	def xml: Elem

	override def toString: String = xml.mkString
}