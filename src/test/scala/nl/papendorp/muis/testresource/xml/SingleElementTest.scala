package nl.papendorp.muis.testresource.xml

import cats.data.ValidatedNec
import nl.papendorp.muis.testresource.TestConfig
import nl.papendorp.muis.testresource.catsextensions.ValidationOps._
import nl.papendorp.muis.xml.{DuplicateTag, MissingTag, SpecificationError, XMLTag}

import scala.xml.{Elem, Node, Null, TopScope}

trait SingleElementTest extends TestConfig
{
	val emptyXML: Elem

	def ensuresSingleElementOf( tag: XMLTag )( implicit actual: Node => ValidatedNec[ SpecificationError, _ ] ): Unit =
	{
		s"report missing ${tag.name} tag" in {
			actual( emptyXML ) reports MissingTag( tag )
		}

		s"report duplicate ${tag.name} tag" in {
			val duplicateElements = actual( duplicateTag( emptyXML, tag ) )
			duplicateElements reports DuplicateTag( tag )
		}
	}

	private def duplicateTag( elem: Elem, xmlTag: XMLTag ): Elem =
	{
		val newTag = Elem( null, xmlTag.name, Null, TopScope, minimizeEmpty = true )
		elem.copy( child = elem.child ++ Seq( newTag, newTag ) )
	}
}
