package nl.papendorp.muis.testresource

import org.scalacheck.Gen
import org.scalacheck.Gen.nonEmptyMap
import org.scalatest.prop.Checkers
import org.scalatest.{Matchers, WordSpec}

import scala.language.implicitConversions

trait TestConfig
	extends WordSpec
		with Matchers
		with Checkers

trait MockGenerators
{
	private implicit def toList[ A ]( iterable: Iterable[ A ] ): List[ A ] = iterable.toList

	private implicit def toSet[ A ]( iterable: Iterable[ A ] ): Set[ A ] = iterable.toSet

	def mapBy[ KEY, VALUE ]( generator: Gen[ VALUE ], key: VALUE => KEY ): Gen[ Map[ KEY, VALUE ] ] =
		nonEmptyMap( for( value <- generator ) yield key( value ) -> value )

	def uniqueBy[ VALUE ]( generator: Gen[ VALUE ], key: VALUE => _ ): Gen[ Set[ VALUE ] ] =
		for( map <- mapBy( generator, key ) ) yield map.values
}