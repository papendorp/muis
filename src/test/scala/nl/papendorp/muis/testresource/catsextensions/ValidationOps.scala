package nl.papendorp.muis.testresource.catsextensions

import cats.Eq
import cats.data.Validated.Invalid
import cats.data.ValidatedNec
import nl.papendorp.muis.testresource.TestConfig
import nl.papendorp.muis.testresource.catsextensions.ChainOps._

import scala.language.implicitConversions

class ValidationOps[ A: Eq ]( validation: ValidatedNec[ A, _ ] ) extends TestConfig
{
	def reports( error: A ): Unit = validation match {
		case Invalid( errors ) => errors should contain( error )
		case valid => fail( s"$valid was valid, but should have reported $error." )
	}

	def contains( error: A ): Boolean = validation match {
		case Invalid( errors ) => errors contains error
		case _ => false
	}
}

object ValidationOps
{
	implicit def validatedSheet[ A: Eq ]( sheet: ValidatedNec[ A, _ ] ): ValidationOps[ A ] =
		new ValidationOps[ A ]( sheet )
}
