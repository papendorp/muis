package nl.papendorp.muis.testresource.catsextensions

import cats.Eq
import cats.data.NonEmptyChain
import org.scalatest.enablers.Containing

object ChainOps
{
	implicit def chainContains[ A: Eq ]: Containing[ NonEmptyChain[ A ] ] = new Containing[ NonEmptyChain[ A ] ]
	{
		override def contains( container: NonEmptyChain[ A ], element: Any ): Boolean = element match {
			case a: A => container.contains( a )
			case _ => false
		}

		override def containsOneOf( container: NonEmptyChain[ A ], elements: Seq[ Any ] ): Boolean =
			elements.count( contains( container, _ ) ) == 1

		override def containsNoneOf( container: NonEmptyChain[ A ], elements: Seq[ Any ] ): Boolean =
			!container.exists( elements.contains( _ ) )
	}
}
