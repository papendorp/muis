package nl.papendorp.muis.character

import cats.data.Validated.{Invalid, Valid}
import cats.data.ValidatedNec
import nl.papendorp.muis.character.CharacterXML.{validXMLs, xmlGenerator}
import nl.papendorp.muis.classes.CharacterClass
import nl.papendorp.muis.race.{MockRace, Race}
import nl.papendorp.muis.rules.{InvalidAlignment, MockAlignmentText, MockRuleBooks, RuleBook}
import nl.papendorp.muis.testresource.catsextensions.ValidationOps._
import nl.papendorp.muis.testresource.xml.{MockXML, ParserChecker, SingleElementTest}
import nl.papendorp.muis.xml.{InvalidMainTag, SpecificationError}
import org.scalacheck.Gen
import org.scalacheck.Gen.{asciiStr, listOf, oneOf => pickOneOf}
import org.scalacheck.Prop.forAll

import scala.language.implicitConversions
import scala.util.Try
import scala.xml.{Elem, Node}

trait MockCharacterClassLevels
{
	def mockClassLevels( classes: Seq[ CharacterClass ] ): Gen[ ClassLevel ] = for {
		anyCharacterClass <- Gen.oneOf( classes )
	} yield ClassLevel( anyCharacterClass )
}

class PlayerCharacterSuite
	extends ParserChecker[ CharacterXML, PlayerCharacter ]
		with SingleElementTest
		with MockRace
		with MockAlignmentText
{
	override val emptyXML: Elem = <character/>

	implicit val actual = ( node: Node ) => CharacterSheet( node )( RuleBook() )

	override def actual( mockXML: CharacterXML ): ValidatedNec[ SpecificationError, PlayerCharacter ] =
		CharacterSheet( mockXML.xml )( mockXML.givenRuleBook )

	"A character" when {
		"loaded from invalid XML" should {
			"complete without exception" in {
				assert( Try( CharacterSheet( emptyXML )( RuleBook() ) ).isSuccess )
			}

			"verify main tag" in {
				actual( <incorrect/> ) reports InvalidMainTag( "incorrect", "character" )
			}

			for( tagName <- Seq( "name", "race", "alignment", "progression" ) ) {
				ensuresSingleElementOf( tagName )
			}
		}

		"loaded from valid XML" should {
			"read name" in {
				checkProperty( _.name == _.expectedName )
			}
		}

		"parsing alignment" should {
			"accept valid alignment" in {
				checkProperty( _.alignment.text === _.expectedAlignmentText )
			}

			"report invalid alignment" in {
				reportsUsing( xml => InvalidAlignment( xml.expectedAlignmentText ) )( xmlGenerator( alignmentTexts = invalidAlignmentTexts ) )
			}
		}

		"parsing race" should {
			"retrieve race from context by name" in {
				checkProperty( _.race === _.expectedRace )
			}

			"report missing race" in {
				val xmlWithUnknownRace = for {
					xml <- validXMLs
					unknownRace <- validRaces
					if xml.givenRuleBook.race( unknownRace.name ).isEmpty
				} yield xml.copy( expectedRace = unknownRace )

				reportsUsing( xml => MissingRace( xml.expectedRace.name ) )( xmlWithUnknownRace )
			}
		}

		"parsing levels" should {
			"retrieve classes from context by name" in {
				checkProperty( _.progression === _.expectedClassProgression )
			}

			"report missing character class" in {
				check( forAll( validXMLs )( xml => {
					val rulesWithoutClasses: RuleBook = xml.givenRuleBook.withClasses() match {
						case Valid( b ) => b
						case _ => throw new IllegalStateException
					}

					val actualSheet = CharacterSheet( xml.xml )( rulesWithoutClasses )
					actualSheet match {
						case Invalid( chain ) => xml.expectedClassProgression.
							map( _.characterClass.name ).
							forall( name => chain contains MissingCharacterClass( name ) )
						case Valid( playerCharacter ) => playerCharacter.progression == Nil
					}
				} ) )
			}
		}
	}
}

case class CharacterXML(
	expectedName: String,
	expectedAlignmentText: String,
	expectedRace: Race,
	expectedClassProgression: List[ ClassLevel ],
	givenRuleBook: RuleBook
) extends MockXML
{
	def xml: Elem =
		<character>
			<name>
				{expectedName}
			</name>
			<alignment>
				{expectedAlignmentText}
			</alignment>
			<race>
				{expectedRace.name}
			</race>
			<progression>
				{for( level <- expectedClassProgression ) yield
				<level>
					{level.characterClass.name}
				</level>}
			</progression>
		</character>
}

object CharacterXML
	extends MockRuleBooks
		with MockAlignmentText
		with MockRace
		with MockCharacterClassLevels
{
	def xmlGenerator(
		ruleBooks: Gen[ RuleBook ] = mockRuleBooks,
		names: Gen[ String ] = asciiStr,
		alignmentTexts: Gen[ String ] = validAlignmentTexts
	): Gen[ CharacterXML ] = for {
		anyRuleBook <- ruleBooks
		anyName <- names
		anyAlignmentText <- alignmentTexts
		anyRace <- pickOneOf( anyRuleBook.races.values.toList )
		anyClassProgression <- listOf( mockClassLevels( anyRuleBook.characterClasses.values.toList ) )
	} yield CharacterXML( anyName, anyAlignmentText, anyRace, anyClassProgression, anyRuleBook )


	implicit val validXMLs: Gen[ CharacterXML ] = xmlGenerator()
}

