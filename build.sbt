name := "muis"

organization := "nl.papendorp"

version := "0.1-SNAPSHOT"

scalaVersion := "2.12.8"

lazy val catsVersion = "1.5.0"

lazy val scalaTestVersion = "3.0.5"
lazy val scalaCheckVersion = "1.14.0"

//unmanagedClasspath in Runtime += baseDirectory.value / "src/main/resources"

scalacOptions += "-Ypartial-unification"

libraryDependencies ++= Seq(
	"org.typelevel" %% "cats-core" % "1.5.0",
	"org.scala-lang.modules" %% "scala-xml" % "1.1.1",

	"org.scalatest" %% "scalatest" % scalaTestVersion % Test,
	"org.scalacheck" %% "scalacheck" % scalaCheckVersion % Test,

	//http://scalamock.org/quick-start/
	"org.scalamock" %% "scalamock-scalatest-support" % "3.5.0" % Test,
)
